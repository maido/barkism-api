/**
 * @prettier
 */
require('dotenv').config();

const IS_PRODUCTION = process.env.NODE_ENV === 'production';

const fs = require('fs');
const express = require('express');
const bodyParser = require('body-parser');
const cors = require('cors');
const helmet = require('helmet');
const morgan = require('morgan');
const routes = require('./routes');

if (IS_PRODUCTION) {
    const bugsnag = require('bugsnag');

    bugsnag.register(process.env.BUGSNAG_API_KEY);
}

const app = express();

app.use(bodyParser.urlencoded({extended: false}));
app.use(bodyParser.json());
app.use(cors());
app.use(helmet());
app.use(
    morgan('common', {
        stream: fs.createWriteStream('../access.log', {flags: 'a'})
    })
);
app.use(routes);

module.exports = app;
