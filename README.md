# Barkism - API

This REST API extends the functionality of the Barkism Shopify site.

[View production site](https://api.barkism.com/).

## ⬇️ Installing / Getting started

The API is installed as part of the main Barkism repository.


## 🛠 Stack

The API runs on a Node server, using the Express framework. 

- Node
- Express 
- [Express Validator](https://www.npmjs.com/package/express-validator), for validating requests
- [CORS](https://www.npmjs.com/package/cors), for managing CORS requests
- [Helmet](https://www.npmjs.com/package/helmet), for securing HTTP headers
- [Body Parser](https://www.npmjs.com/package/body-parser), for parsing JSON and URL Encoded request bodies
- [Morgan](https://www.npmjs.com/package/morgan), for logging requests
- [New Relic](https://www.npmjs.com/package/newrelic), for monitoring performance
- [BugSnag](https://www.npmjs.com/package/bugsnag), for error reporting



## 🖥 Developing

### Running server

To get the server up and running, simply run:

```shell
npm start
```

This will create a local server with the default port of **1337**. This relies on `.env` file that contains all required environment variables for the APIs, etc. If you need a copy from production please work with DevOps to complete this.

### Testing

Tests are currently missing. Please check back soon.


### Deploying / Publishing

Deployments are currently made manually via Deploybot. Please work with DevOps to complete this.


## 👋 Contributing

Please see README of the main project for contribution guidelines.
