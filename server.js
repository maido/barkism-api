/**
 * @prettier
 */
require('dotenv').config();

const IS_HTTPS = process.env.IS_HTTPS === 'true';

const fs = require('fs');
const app = require('./app');

const port = process.env.PORT || 1337;
let server;

if (IS_HTTPS) {
    const https = require('https');
    const credentials = {
        key: fs.readFileSync('./ssl/key.pem'),
        cert: fs.readFileSync('./ssl/cert.pem'),
        passphrase: 'sslyo'
    };

    server = https.createServer(credentials, app);
} else {
    const http = require('http');

    server = http.createServer(app);
}

server.listen(port);
server.on('listening', () => console.log(`Server listening at localhost:${port}`));

module.exports = app;
