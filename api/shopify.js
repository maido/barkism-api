/**
 * @prettier
 */
require('dotenv').config();
const faker = require('faker');
const shopifyAPI = require('shopify-api-node');
const find = require('lodash/find');
const {discountCodes, dogBreeds, filterMetafield, random} = require('./helpers');

const shopify = new shopifyAPI({
    shopName: process.env.SHOPIFY_STORE_NAME,
    apiKey: process.env.SHOPIFY_APP_API_KEY,
    password: process.env.SHOPIFY_APP_PASSWORD
});

const updateType = async (type, method, data, id) => {
    const request = await shopify[type][method](id ? id : data, id ? data : null);

    return request;
};

const updateMetafields = async (method, data) => {
    if (data instanceof Array) {
        const requests = data.filter(filterMetafield).map(d => shopify.metafield[method](d));

        return Promise.all(requests);
    } else {
        if (filterMetafield(data)) {
            const request = await shopify.metafield[method](data);

            return request;
        }
    }
};

const setCustomerDefaultDogInformation = async customerID => {
    const orders = await shopify.customer.orders(customerID);
    const ordersProperties = orders.map(o => {
        const hasProperties = o.line_items.length && o.line_items[0].properties;

        if (hasProperties) {
            const name = find(o.line_items[0].properties, ['name', 'dog_name']);
            const breed = find(o.line_items[0].properties, ['name', 'dog_breed']);

            if (name && breed) {
                return {name: name.value, breed: breed.value};
            }
        }
    });

    if (ordersProperties.length >= 1) {
        const metafields = [
            {
                owner_id: customerID,
                owner_resource: 'customer',
                namespace: 'dogdetails',
                value_type: 'string',
                key: 'breed',
                value: ordersProperties[0].breed
            },
            {
                owner_id: customerID,
                owner_resource: 'customer',
                namespace: 'dogdetails',
                value_type: 'string',
                key: 'name',
                value: ordersProperties[0].name
            }
        ];
        const updates = await updateMetafields('create', metafields);

        return updates;
    }

    return '';
};

const deleteAllOrders = async () => {
    const orders = await shopify.order.list({fields: 'customer,id,tags', limit: 250});
    const generatedOrders = orders.filter(o => o.tags.includes('data-generator'));

    if (generateOrders.length) {
        await Promise.all(generatedOrders.map(order => shopify.order.delete(order.id)));

        const customerIDs = generatedOrders.map(o => o.customer.id);
        const customersToDelete = customerIDs.map(id => shopify.customer.delete(id));

        return Promise.all(customersToDelete);
    } else {
        return 'no orders to remove';
    }
};

const generateOrders = async count => {
    const products = await shopify.product.list();

    const ordersToCreate = Array.apply(null, {length: count}).map(() => {
        const product = random(products);
        const firstName = faker.name.firstName();
        const lastName = faker.name.lastName();
        const email = faker.internet.email();

        let order = {
            email: email,
            customer: {
                first_name: firstName,
                last_name: lastName
            },
            line_items: [
                {
                    variant_id: random(product.variants).id,
                    quantity: 1,
                    properties: [
                        {name: 'dog_name', value: firstName},
                        {name: 'dog_breed', value: random(dogBreeds)},
                        {name: 'message', value: faker.lorem.sentence()}
                    ]
                }
            ],
            send_receipt: false,
            send_fulfillment_receipt: false,
            shipping_address: {
                first_name: firstName,
                last_name: lastName,
                address1: faker.address.streetAddress(),
                phone: faker.phone.phoneNumber(),
                city: faker.address.city(),
                province: faker.address.state(),
                country: faker.address.country(),
                zip: faker.address.zipCode()
            },
            tags: ['data-generator']
        };

        if (Math.random() * 10 >= 5) {
            order.discount_codes = [
                {code: random(discountCodes), amount: '2.50', type: 'fixed_amount'}
            ];
        }

        return shopify.order.create(order);
    });

    return Promise.all(ordersToCreate);
};

module.exports = {
    deleteAllOrders,
    generateOrders,
    setCustomerDefaultDogInformation,
    updateMetafields,
    updateType
};
