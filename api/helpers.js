/**
 * @prettier
 */

const discountCodes = ['CHIPTHEDAXI18', 'SAMSCHEME', 'SHUN7', 'SHEILA9', 'GEORGE9'];

const dogBreeds = [
    'Chihuahua',
    'Shih Tzu',
    'Pug',
    'Pomeranian',
    'King Charles Spaniel',
    'Bichon Frise',
    'French Bulldog',
    'Dachshund',
    'Corgi'
];

const random = collection => collection[Math.floor(Math.random() * (collection.length - 1))];

const filterMetafield = field => {
    if (field.value && field.namespace && field.key) {
        return field;
    }
};

module.exports = {
    discountCodes,
    dogBreeds,
    random,
    filterMetafield
};
