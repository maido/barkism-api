/**
 * @prettier
 */
const request = require('supertest');
const nock = require('nock');
const app = require('../app');

const API_URL = `https://${process.env.SHOPIFY_STORE_NAME}.myshopify.com`;

describe('App', () => {
    test('it return a redirect message for the base path', async () => {
        const response = await request(app).get('/');

        expect(response.statusCode).toEqual(200);
        expect(response.body).toEqual('go to https://www.barkism.com');
    });
});

describe('Shopify', () => {
    describe('metafields->create', () => {
        test('it does not support get', async () => {
            const response = await request(app).get('/metafields/create');

            expect(response.statusCode).toEqual(404);
        });

        test('it returns an error for an empty post', async () => {
            const response = await request(app).post('/metafields');

            expect(response.statusCode).toEqual(400);
            expect(response.body.errors.metafields.msg).toEqual(
                'Please provide at least one metafield'
            );
        });

        test('it returns an error for invalid types', async () => {
            const response = await request(app)
                .post('/metafields')
                .send({metafields: 1});

            expect(response.statusCode).toEqual(400);
            expect(response.body.errors.metafields.msg).toEqual(
                'Please provide at least one metafield'
            );
        });

        test('it returns an error for invalid schemas', async () => {
            const response = await request(app)
                .post('/metafields')
                .set('Accept', 'application/json')
                .send({
                    metafields: {namespace: 'test', key: 'test', value: 1}
                });

            expect(response.statusCode).toEqual(400);
            expect(response.body.errors.metafields.msg).toEqual(
                'Please provide a valid metafield scheme'
            );
        });

        test('it returns a response for a valid schema for a single metafield', async () => {
            const data = {
                owner_id: 1,
                owner_resource: 'test',
                namespace: 'test',
                key: 'test',
                value: 'test',
                value_type: 'string'
            };

            nock(API_URL)
                .post('/admin/metafields.json', {metafield: data})
                .reply(200, {metafield: data});

            const response = await request(app)
                .post('/metafields')
                .set('Accept', 'application/json')
                .send({metafields: data});

            expect(response.statusCode).toEqual(200);
            expect(response.body).toEqual(data);
        });

        test('it returns a response for a valid schema for an array of metafields', async () => {
            const data = [
                {
                    owner_id: 1,
                    owner_resource: 'test',
                    namespace: 'test',
                    key: 'test',
                    value: 'test',
                    value_type: 'string'
                },
                {
                    owner_id: 2,
                    owner_resource: 'test',
                    namespace: 'test',
                    key: 'test',
                    value: 'test',
                    value_type: 'string'
                }
            ];

            nock(API_URL)
                .post('/admin/metafields.json', {metafield: data[0]})
                .reply(200, {metafield: data[0]});
            nock(API_URL)
                .post('/admin/metafields.json', {metafield: data[1]})
                .reply(200, {metafield: data[1]});

            const response = await request(app)
                .post('/metafields')
                .set('Accept', 'application/json')
                .send({metafields: data});

            expect(response.statusCode).toEqual(200);
            expect(response.body).toEqual(data);
        });
    });
});
