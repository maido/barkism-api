/**
 * @prettier
 */
const {checkSchema, body, param, validationResult} = require('express-validator/check');
const isEqual = require('lodash/isEqual');
const filter = require('lodash/filter');

const returnBadResponse = (response, errors, statusCode = 400) => {
    if (response) {
        // console.log(response);
    }

    return response.status(statusCode).json({errors});
};

const handleValidationErrors = (request, response, next) => {
    const errors = validationResult(request);

    if (!errors.isEmpty()) {
        return returnBadResponse(response, errors.mapped());
    } else {
        next();
    }
};

const isValidCustomerRequest = () => {
    return param('id', 'Please provide a valid customer ID')
        .exists()
        .isNumeric();
};

const isNonEmptyMetafieldsRequest = () => {
    return body('metafields', 'Please provide at least one metafield')
        .exists()
        .custom(value => {
            return typeof value === 'object' || value instanceof Array === true;
        });
};

const isValidMetafieldsRequest = () => {
    const requiredKeys = ['key', 'namespace', 'owner_id', 'owner_resource', 'value', 'value_type'];

    return body('metafields', 'Please provide a valid metafield scheme').custom(value => {
        if (value instanceof Array) {
            return (
                filter(value, v => isEqual(requiredKeys, Object.keys(v).sort())).length ===
                value.length
            );
        } else {
            return isEqual(requiredKeys, Object.keys(value).sort());
        }
    });
};

const isValidMetafieldsSchema = () => {
    return checkSchema({
        namespace: {
            isLength: {
                errorMessage: 'The namespace should be between 1 and 20 characters',
                options: {min: 1, max: 20}
            }
        },
        key: {
            isLength: {
                errorMessage: 'The key should be between 1 and 30 characters',
                options: {min: 1, max: 30}
            }
        },
        value: {
            isLength: {
                errorMessage: 'The value should be defined',
                options: {min: 1, max: 10000}
            }
        },
        owner_resource: {
            isString: true,
            errorMessage: 'The owner_resource should be defined'
        },
        owner_id: {
            isInt: true,
            errorMessage: 'The owner_id should be defined'
        }
    });
};

module.exports = {
    handleValidationErrors,
    isValidCustomerRequest: isValidCustomerRequest(),
    isValidMetafieldsRequest: [isNonEmptyMetafieldsRequest(), isValidMetafieldsRequest()],
    returnBadResponse
};
