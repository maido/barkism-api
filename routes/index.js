/**
 * @prettier
 */
const {Router} = require('express');
const {
    isValidMetafieldsRequest,
    handleValidationErrors,
    returnBadResponse
} = require('./validation');
const ShopifyAPI = require('../api/shopify');

const router = Router();

router.get('/', (req, res) => res.json('go to https://www.barkism.com'));

router.post('/customers/set-default-dog-information/:id', (req, res) => {
    const {id} = req.params;

    ShopifyAPI.setCustomerDefaultDogInformation(id)
        .then(data => res.json(data))
        .catch(error => returnBadResponse(res, [error]));
});

router.post('/metafields', [isValidMetafieldsRequest, handleValidationErrors], (req, res) => {
    const {metafields} = req.body;

    ShopifyAPI.updateMetafields('create', metafields)
        .then(data => res.json(data))
        .catch(error => returnBadResponse(res, [error]));
});

router.post('/data-generator/create/orders/:count', (req, res) => {
    const {count} = req.params;

    ShopifyAPI.generateOrders(count)
        .then(data => res.json(data))
        .catch(error => returnBadResponse(res, [error]));
});

router.post('/data-generator/delete/orders', (req, res) => {
    ShopifyAPI.deleteAllOrders()
        .then(data => res.json(data))
        .catch(error => returnBadResponse(res, [error]));
});

module.exports = router;
